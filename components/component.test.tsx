import React from 'react';
import { render, screen } from '@testing-library/react';
import TestComponent from '@/components/component';

describe('TestComponent', () => {
  it('render', () => {
    render(<TestComponent />);
    expect(screen.getByText('TestComponent')).toBeInTheDocument();
  });
});
